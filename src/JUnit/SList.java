/*
 * Name:       Felipe Monteiro
 * Course:     Computer Science 215
 * Instructor: Dr. Hochrine
 * Date:       10/25/2013
 * 
 * Project 2: Specifications, Implementations, and Test Cases (Oh My)!
 * 
 * Comment:    Program that uses vectors to model string of Objects. The
 * 			   purpose of this program was to learn how to create an 
 * 			   implementation from a pre-designed interface along with
 * 			   appropriate specifications. In addition, we had to 
 *             translate English specifications into mathematical
 *             or formal specifications. 
 *             
 *             Every precondition is assumed to be true. Thus, extra 
 *             conditional checks have not been included to prevent
 *             the program from crashing, in the event that the precondition
 *             is violated on the part of the user. addRight() is 
 *             presumably the only method whose precondition could be 
 *             observed while still causing the program to crash by 
 *             passing null to the method. This case is specially
 *             handled.
 */

package JUnit;

public interface SList {
	// modeled by: (left: string of object, right: string of object)
	// initial value: (<>,<>)
	
	void clear();
	/*! 
	 ** precondition: 
	 **		true
	 ** postcondition:
	 **		self = (<>,<>)
	!*/
	
	void addRight(Object x);
	/*!
	 ** precondition: 
	 ** 	true
	 ** postcondition: 
	 **		self.right = <x> * #self.right AND
	 **		x = #x
	!*/
	
	Object removeRight();
	/*!
	 ** precondition: 
	 ** 	|self.right| > 0 
	 ** postcondition: 
	 ** 	#self.right = <removeRight()> * self.right
	!*/
	
	Object getElementAt(int pos);
	/*!
	 ** precondition: 
	 **		0 <= pos < |self.right|
	 ** preserves:
	 **		self, pos
	 ** postcondition: 
	 ** 	there exist x : Object; l, r : string of Object
	 ** 	s.t.
	 ** 	#self.right = l * <x> * r AND
	 ** 	|l| = pos AND
	 ** 	getElementAt() = x
	!*/
	
	void advance();
	/*!
	 ** precondition: 
	 **		|self.right| > 0
	 ** postcondition: 
	 **		#self.right = <removeRight()> * self.right AND
	 **		self.left = #self.left * <removeRight()> 
	!*/
	
	void moveToStart();
	/*!
	 ** precondition: 
	 **		true
	 ** postcondition: 
	 **		self.right = self.left * #self.right AND
	 ** 	self.left = <>		
	!*/
	
	void moveToFinish();
	/*!
	 ** precondition: 
	 **		true
	 ** postcondition: 
	 ** 	self.left = #self.left * self.right AND
	 **		self.right = <>
	!*/
	
	int getLeftLength();
	/*!
	 ** precondition: 
	 **		true
	 ** postcondition: 
	 **		getLeftLength() = |self.left| AND
	 **		self = #self
	!*/
	
	int getRightLength();
	/*!
	 ** precondition:
	 **		 true
	 ** postcondition: 
	 **		getRightLength() = |self.right| AND
	 **		self = #self
	!*/ 
	
	String abstractToString();
}
