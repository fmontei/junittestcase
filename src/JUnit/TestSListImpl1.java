package JUnit;

import org.junit.Before;
import junit.framework.*;

public class TestSListImpl1 extends TestCase {
	
	private SList s0, s1, s2, s3, s4, s5;
	
	@Before
	public void setUp() {
		
		s0 = new SListImpl1();
		s1 = new SListImpl1();
		s2 = new SListImpl1();
		s3 = new SListImpl1();
		s4 = new SListImpl1();
		s5 = new SListImpl1();
		
		s1.addRight(1);
		
		s2.addRight(1);
		s2.addRight(2);
		
		s3.addRight(1);
		s3.addRight(2);
		s3.addRight(3);
		
		for (int i = 0; i < 10; ++i)
			s4.addRight(new Integer(i));
		
		s4.advance();
		s4.advance();
		s4.advance();
		
		s5.addRight("J");
		s5.addRight("a");
		s5.addRight("v");
		s5.addRight("a");
		
		s5.advance();
	}
	
	public void testClear() {
		
		s0.clear();
		assertEquals("(<>,<>)", s0.abstractToString());
		
		assertEquals("(<>,<1>)", s1.abstractToString());
		s1.clear();
		assertEquals("(<>,<>)", s1.abstractToString());
		assertNotSame("<>,<1>", s1.abstractToString());
		
		s2.clear();
		assertEquals("(<>,<>)", s2.abstractToString());
		
		s3.clear();
		assertEquals("(<>,<>)", s3.abstractToString());
	}
	
	public void testAddRight() {
		
		assertEquals("(<>,<>)", s0.abstractToString());
		assertEquals("(<>,<1>)", s1.abstractToString());
		assertEquals("(<>,<2,1>)", s2.abstractToString());
		assertEquals("(<>,<3,2,1>)", s3.abstractToString());
		
		assertEquals("(<>,<>)", s0.abstractToString());
		s0.addRight(null);
		assertEquals("(<>,<>)", s0.abstractToString());
		
		s0.addRight(1);
		assertEquals("(<>,<1>)", s0.abstractToString());
		
		s0.addRight(2);
		assertEquals("(<>,<2,1>)", s0.abstractToString());
		
		s0.addRight(5);
		assertEquals("(<>,<5,2,1>)", s0.abstractToString());
	}
	
	public void testRemoveRight() {
		
		Object i = s1.removeRight();
		assertNotNull(i);
		assertEquals("1", i.toString());
		assertEquals("(<>,<>)", s1.abstractToString());
		
		i = s2.removeRight();
		assertNotNull(i);
		assertEquals("2", i.toString());
		assertEquals("(<>,<1>)", s2.abstractToString());
		i = s2.removeRight();
		assertNotNull(i);
		assertEquals("1", i.toString());
		assertEquals("(<>,<>)", s2.abstractToString());
		
		i = s3.removeRight();
		assertNotNull(i);
		assertEquals("3", i.toString());
		assertEquals("(<>,<2,1>)", s3.abstractToString());
		i = s3.removeRight();
		assertNotNull(i);
		assertEquals("2", i.toString());
		assertEquals("(<>,<1>)", s3.abstractToString());
		i = s3.removeRight();
		assertNotNull(i);
		assertEquals("1", i.toString());
		assertEquals("(<>,<>)", s3.abstractToString());
	}
	
	public void testGetElementAt() { // Edit this
		
		Object i = s1.getElementAt(0);
		assertNotNull(i);
		assertEquals("1", i.toString());
		
		i = s3.getElementAt(0);
		assertNotNull(i);
		assertEquals("3", i.toString());
		i = s3.getElementAt(1);
		assertNotNull(i);
		assertEquals("2", i.toString());
		i = s3.getElementAt(2);
		assertNotNull(i);
		assertEquals("1", i.toString());
	}
	
	public void testAdvance() {
		
		assertEquals("(<>,<1>)", s1.abstractToString());
		s1.advance();
		assertEquals("(<1>,<>)", s1.abstractToString());
		
		assertEquals("(<>,<2,1>)", s2.abstractToString());
		s2.advance();
		assertEquals("(<2>,<1>)", s2.abstractToString());
		s2.advance();
		assertEquals("(<2,1>,<>)", s2.abstractToString());
		
		assertEquals("(<>,<3,2,1>)", s3.abstractToString());
		s3.advance();
		assertEquals("(<3>,<2,1>)", s3.abstractToString());
		s3.advance();
		assertEquals("(<3,2>,<1>)", s3.abstractToString());
		s3.advance();
		assertEquals("(<3,2,1>,<>)", s3.abstractToString());
		
		s4.clear();
		s5.clear();
		
		for (int i = 0; i < 10; ++i)
			s4.addRight(new Integer(i));
		
		assertEquals("(<>,<9,8,7,6,5,4,3,2,1,0>)", s4.abstractToString());
		s4.advance();
		assertEquals("(<9>,<8,7,6,5,4,3,2,1,0>)", s4.abstractToString());
		s4.advance();
		assertEquals("(<9,8>,<7,6,5,4,3,2,1,0>)", s4.abstractToString());
		s4.advance();
		assertEquals("(<9,8,7>,<6,5,4,3,2,1,0>)", s4.abstractToString());
		
		s5.addRight("J");
		s5.addRight("a");
		s5.addRight("v");
		s5.addRight("a");
		
		assertEquals("(<>,<a,v,a,J>)", s5.abstractToString());
		s5.advance();
		assertEquals("(<a>,<v,a,J>)", s5.abstractToString());
	}
	
	public void testMoveToStart() {
		
		assertEquals("(<>,<>)", s0.abstractToString());
		s0.moveToStart();
		assertEquals("(<>,<>)", s0.abstractToString());
		
		assertEquals("(<>,<2,1>)", s2.abstractToString());
		s2.moveToStart();
		assertEquals("(<>,<2,1>)", s2.abstractToString());
		
		assertEquals("(<9,8,7>,<6,5,4,3,2,1,0>)", s4.abstractToString());
		s4.moveToStart();
		assertEquals("(<>,<9,8,7,6,5,4,3,2,1,0>)", s4.abstractToString());
		
		assertEquals("(<a>,<v,a,J>)", s5.abstractToString());
		s5.moveToStart();
		assertEquals("(<>,<a,v,a,J>)", s5.abstractToString());
	}
	
	public void testMoveToFinish() {
		
		assertEquals("(<>,<>)", s0.abstractToString());
		s0.moveToFinish();
		assertEquals("(<>,<>)", s0.abstractToString());
		
		assertEquals("(<>,<2,1>)", s2.abstractToString());
		s2.moveToFinish();
		assertEquals("(<2,1>,<>)", s2.abstractToString());
	
		assertEquals("(<9,8,7>,<6,5,4,3,2,1,0>)", s4.abstractToString());
		s4.moveToFinish();
		assertEquals("(<9,8,7,6,5,4,3,2,1,0>,<>)", s4.abstractToString());
		
		assertEquals("(<a>,<v,a,J>)", s5.abstractToString());
		s5.moveToFinish();
		assertEquals("(<a,v,a,J>,<>)", s5.abstractToString());
	}
	
	public void testGetLeftLength() {
		
		int i = s0.getLeftLength();
		assertEquals(0, i);
		
		i = s1.getLeftLength();
		assertEquals(0, i);
		
		i = s2.getLeftLength();
		assertEquals(0, i);
		
		i = s3.getLeftLength();
		assertEquals(0, i);
		
		i = s4.getLeftLength();
		assertEquals(3, i);
		
		i = s5.getLeftLength();
		assertEquals(1, i);
	}
	
	public void testGetRightLength() {
		
		int i = s0.getRightLength();
		assertEquals(0, i);
		
		i = s1.getRightLength();
		assertEquals(1, i);
		
		i = s2.getRightLength();
		assertEquals(2, i);
		
		i = s3.getRightLength();
		assertEquals(3, i);
		
		i = s4.getRightLength();
		assertEquals(7, i);
		
		i = s5.getRightLength();
		assertEquals(3, i);
	}
}
