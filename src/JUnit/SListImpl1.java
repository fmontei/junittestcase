package JUnit;

import java.util.Vector;

public class SListImpl1 implements SList {

	// The left and right strings are abstractions; they are modeled by vectors
	Vector<Object> left  = new Vector<Object>();
	Vector<Object> right = new Vector<Object>();
	
	public void clear() {
		
		left.clear();
		right.clear();
	}

	public void addRight(Object x) {
		
		// The precondition states that addRight() may be called at any time;
		// therefore, the user could pass null to addRight() without violating
		// the contract, which would likely cause the program to crash
		if (x == null) return;
		
		right.add(0, x);
	}

	public Object removeRight() {
		
		Object temp;
		
		temp = right.get(0);
		right.remove(0);
		return temp;
	}

	// Contract-by-design means that the precondition must be respected
	// by the user; therefore, an if statement that performs the following
	// checks: 0 <= pos < |self.right| is superfluous
	public Object getElementAt(int pos) {
		
		return right.get(pos);
	}

	// Appends leftmost object in right to left vector
	public void advance() {
		
		Object o = removeRight();
		left.add(left.size(), o);
	}

	// Appends right string to left string
	public void moveToStart() {
		
		int i = left.size() - 1;
		while (i >= 0) {
			
			addRight(left.get(i--));
		}
		
		left.clear();
	}

	// Prepends left string to right string
	public void moveToFinish() {
		
		int i = 0;
		while (i < right.size()) {
			
			left.add(left.size(), right.get(i++));
		}
		
		right.clear();
	}

	public int getLeftLength() {
		
		return left.size();
	}
	
	public int getRightLength() {
		
		return right.size();
	}

	// Concatenates left and right vectors' strings together
	public String abstractToString() {
		
		String result = "(<";
		
		// Iterate over left vector
		for (Object o : left)
			result += o.toString() + ",";
		
		// Ensures that if the vector's size is zero, an additional comma isn't
		// appended to the end of the return string
		if (left.size() > 0)
			result = result.substring(0, result.length() - 1);
		
		result += ">,<";
		
		// Iterate over right vector
		for (Object o : right)
			result += o.toString() + ",";
		
		if (right.size() > 0)
			result = result.substring(0, result.length() - 1);
		
		result += ">)";
		
		return result;
	}
}
